const express = require('express')
const app = express()
const port = process.env.PORT || 8080;

app.use(express.urlencoded({ extended: false }));

const cors = require('cors');
app.use(cors());

app.listen(port);

app.get('/', (req, res) => {
    res.send('List of movies');
});

/*app.get('/list', (req, res) => {
    let val = req.query.number;

    let listt = 'None';

    if(val==1) {
        listt = 'Asuran, 96, Vadachennai, PaavaKadhaigal';
    }else if(val==2) {
            listt = 'Fight club, The Shashank Redemption, The Prestige, The pursuit of happiness';
    }else if(val==3) {
            listt = 'PK, 3 Idiots, 2 States';
    }else if(val==4) {
            listt = 'Awe, Dj Tillu, Bheemla nayak, C/o Kancherapalem, EE nagaraniki emaindi';
    }else if(val==5) {
            listt = 'Ayyappanum Koshiyum, Kumbalangi nights, Solo, Charlie, Vikramadithyan';
    }
        

    res.send(listt);*/

    app.get('/bodymass', (req, res) => {
        var height = req.query.height;
        var weight = req.query.weight;
        var bmi = (weight/(Math.sqrt(height)))*703;
        res.send(bmi);

});